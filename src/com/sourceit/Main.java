package com.sourceit;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Map<String, Integer> menu = new HashMap<>();
        menu.put("salat", 5);
        menu.put("soup", 15);
        menu.put("pelmeni", 25);


        Map<String, Integer> order = new HashMap<>();
        Scanner in = new Scanner(System.in);

        String next;
        enter : do {
            System.out.print("Enter type: ");
            String type = in.nextLine();
            System.out.print("Enter amount: ");
            int amount = in.nextInt();
            in.nextLine();
            order.put(type, amount);

            do {
                System.out.println("Next or exit? N/E");
                next = in.nextLine();

                if ("N".equals(next)) {
                    break;
                } else if ("E".equals(next)){
                    break enter;
                } else {
                    System.out.println("try again");
                }
            } while (true);

        } while (true);

        System.out.println();
        System.out.println("---");
        int total = 0;
        for (Map.Entry<String, Integer> entry : order.entrySet()) {
            int sum = menu.get(entry.getKey()) * entry.getValue();
            System.out.println(entry.getKey() + ": " + sum);
            total += sum;
        }
        System.out.println("итого: " + total);


    }
}
